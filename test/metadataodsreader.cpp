/**
 * \file /input/mcqr/metadataodsreader.cpp
 * \date 14/09/2023
 * \author Olivier Langella
 * \brief read metadata ods file
 */

/*******************************************************************************
 * Copyright (c) 2023 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *McqrMetadata
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "metadataodsreader.h"

MetadataOdsReader::MetadataOdsReader()
{
}

MetadataOdsReader::~MetadataOdsReader()
{
}

void
MetadataOdsReader::endDocument()
{
}
void
MetadataOdsReader::endLine()
{
}
void
MetadataOdsReader::endSheet()
{
  m_isContent = true;
}

void
MetadataOdsReader::setCell(const OdsCell &cell)
{
  qDebug();
  if(!m_isContent)
    {
      if(cell.isString())
        {
          qDebug();
          m_headers << cell.getStringValue();
        }
    }
  else
    {
      if(m_column < (std::size_t)m_headers.size())
        {
          QString current_column_name = m_headers[m_column];
          if(current_column_name == "msrun")
            {
              qDebug();
              if(!cell.isEmpty())
                {
                  qDebug() << cell.getStringValue();
                }
            }
          else if(current_column_name == "msrunfile")
            {
              qDebug();
              if(!cell.isEmpty())
                {
                  qDebug() << cell.getStringValue();
                }
            }
          else
            {
              qDebug();
              if(!cell.isEmpty())
                {
                  qDebug() << current_column_name << " " << cell.toString();
                }
            }
        }
    }
  m_column++;
  qDebug();
}

void
MetadataOdsReader::startLine()
{
  m_column = 0;
  if(m_headers.size() == 0)
    { // header
    }
  else
    {

      m_isContent = true;
    }
}
void
MetadataOdsReader::startSheet(const QString &sheet_name [[maybe_unused]])
{
}
