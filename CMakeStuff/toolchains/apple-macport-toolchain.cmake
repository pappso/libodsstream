message("APPLE macport environment")
message("Please run the configuration like this:")
message("cmake -DCMAKE_BUILD_TYPE=Debug ../development")


set(CMAKE_C_IMPLICIT_INCLUDE_DIRECTORIES "/opt/local/include")
set(CMAKE_CXX_IMPLICIT_INCLUDE_DIRECTORIES "/opt/local/lib")


set(HOME_DEVEL_DIR "/Users/rusconi/devel")


set(LINKER_FLAGS "${LINKER_FLAGS} -Wc++17-compat")

set(CMAKE_MACOSX_RPATH 0)


find_package(ZLIB REQUIRED)


find_package(QuaZip QUIET)
if(NOT QUAZIP_FOUND)
	message(STATUS "QuaZip not yet found. Searching for it.")
	set(QuaZip_DIR ${CMAKE_MODULE_PATH})
	find_package(QuaZip REQUIRED)
endif()



