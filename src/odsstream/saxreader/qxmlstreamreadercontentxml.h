/**
 * \file src/saxreader/qxmlstreamreadercontentxml.h
 * \date 28/4/2022
 * \author Olivier Langella
 * \brief ODS content XML parser
 */

/*******************************************************************************
 * Copyright (c) 2022 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of libodsstream.
 *
 *     libodsstream is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     libodsstream is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with libodsstream.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


#pragma once

#include <QXmlStreamReader>
#include "../odsdocreader.h"

/**
 * @todo write docs
 */
class QXmlStreamReaderContentXml : public QXmlStreamReader
{
  public:
  /**
   * @todo write docs
   */
  QXmlStreamReaderContentXml(OdsDocReader &ods_reader);

  /**
   * @todo write docs
   */
  virtual ~QXmlStreamReaderContentXml();

  bool read();

  private:
  bool readTable();
  bool readTableRow();
  bool readTableLine();
  bool readCellContent();
  bool readTextP();

  private:
  OdsDocReader &m_odsReader;

  OdsCell m_currentCell;

  uint m_numberColumnsRepeated;
};
