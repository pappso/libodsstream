# File:///home/langella/developpement/git/pappsomspp/CMakeStuff/toolchains/mxe-toolchain.cmake# 
# This file should be included if the command line reads like this:
# x86_64-w64-mingw32.shared-cmake -DCMAKE_BUILD_TYPE=Release -DMXE=1 ..
# export PATH=/backup2/mxeqt6/usr/bin:/usr/local/bin:/usr/bin:/bin:/usr/local/games:/usr/games:/home/langella/.dotnet/tools
# /backup2/mxe/usr/bin/x86_64-w64-mingw32.shared-cmake -DCMAKE_BUILD_TYPE=Release -DMXE=1 ..

MESSAGE("MXE (M cross environment) https://mxe.cc/")
message("Please run the configuration like this:")
message("x86_64-w64-mingw32.shared-cmake -DMXE=1 -G \"Unix Makefiles\" -DCMAKE_BUILD_TYPE=Release ..")


#set(CMAKE_C_IMPLICIT_INCLUDE_DIRECTORIES ${HOME_DEVEL_DIR}/mxe/usr/x86_64-w64-mingw32.shared/include)
#set(CMAKE_CXX_IMPLICIT_INCLUDE_DIRECTORIES ${HOME_DEVEL_DIR}/mxe/usr/x86_64-w64-mingw32.shared/include)


if(WIN32 OR _WIN32)
	message(STATUS "Building with WIN32 defined.")
endif()


find_package(ZLIB REQUIRED)


set(QUAZIP_FOUND 1)
set(QUAZIP_INCLUDE_DIR "/backup2/win64qt6/libquazip1-qt6-1.4/quazip")
set(QUAZIP_LIBRARIES "/backup2/win64qt6/libquazip1-qt6-1.4/build/quazip/libquazip1-qt6.dll")
set(QUAZIP_ZLIB_INCLUDE_DIR ${ZLIB_INCLUDE_DIRS})
set(QUAZIP_INCLUDE_DIRS ${QUAZIP_INCLUDE_DIR} ${QUAZIP_ZLIB_INCLUDE_DIR})


message(STATUS "QUAZIP_INCLUDE_DIR :${QUAZIP_INCLUDE_DIR}")

if(NOT TARGET QuaZip::QuaZip)
	add_library(QuaZip::QuaZip UNKNOWN IMPORTED)
	set_target_properties(QuaZip::QuaZip PROPERTIES
		IMPORTED_LOCATION             ${QUAZIP_LIBRARIES}
		INTERFACE_INCLUDE_DIRECTORIES ${QUAZIP_INCLUDE_DIR})
endif()
