# Copyright : Olivier Langella (CNRS)
# License : GPL-3.0+
# Authors : Olivier Langella

find_path(OdsStream_INCLUDE_DIRS odsstream/odsdocreader.h
	PATHS /usr/local/include /usr/include
	PATH_SUFFIXES odsstream libodsstream ENV PATH)

find_library(OdsStream_LIBRARY NAMES odsstream)

if(OdsStream_INCLUDE_DIRS AND OdsStream_LIBRARY)
	message(STATUS "~~~~~~~~~~~~~ ${OdsStream_LIBRARY} ~~~~~~~~~~~~~~~")
	set(OdsStream_FOUND TRUE)
endif()

if(OdsStream_FOUND)
	if (NOT OdsStream_FIND_QUIETLY)
		message(STATUS "Found OdsStream_LIBRARY: ${OdsStream_LIBRARY}")
	endif(NOT OdsStream_FIND_QUIETLY)
endif(OdsStream_FOUND)

