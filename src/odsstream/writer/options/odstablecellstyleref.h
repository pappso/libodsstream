/**
 * \file odsstream/writer/options/odstablecellstyleref.h
 * \date 10/03/2016
 * \author Olivier Langella
 * \brief internal reference on handler for ODS cell style
 */

/*
    libodsstream is a library to read and write ODS documents as streams
    Copyright (C) 2013  Olivier Langella <Olivier.Langella@moulon.inra.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#ifndef ODSTABLECELLSTYLEREF_H
#define ODSTABLECELLSTYLEREF_H

#include <QString>

class ContentXml;
class OdsTableCellStyleRefInternal;

typedef OdsTableCellStyleRefInternal *OdsTableCellStyleRef;


class OdsTableCellStyleRefInternal
{
  friend class ContentXml;

  private:
  OdsTableCellStyleRefInternal(unsigned int ref_num);


  QString _ref_str;
};

#endif // ODSTABLECELLSTYLEREF_H
