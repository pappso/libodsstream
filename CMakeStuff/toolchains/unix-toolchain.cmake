message("UNIX non APPLE environment")
message("Please run the configuration like this:")
message("cmake -G \"Unix Makefiles\" -DCMAKE_BUILD_TYPE=Debug ../development")

set(CMAKE_C_IMPLICIT_INCLUDE_DIRECTORIES /usr/include)
set(CMAKE_CXX_IMPLICIT_INCLUDE_DIRECTORIES /usr/include)


if(NOT CMAKE_INSTALL_PREFIX)
	set(CMAKE_INSTALL_PREFIX /usr/local)
endif(NOT CMAKE_INSTALL_PREFIX)


find_package(ZLIB REQUIRED)


find_package(Catch2)
message("Catch2 major version found: " ${Catch2_VERSION_MAJOR})
add_compile_definitions(CATCH2_MAJOR_VERSION_${Catch2_VERSION_MAJOR})


find_package(QuaZip-Qt6 REQUIRED)


add_definitions(-fPIC)

