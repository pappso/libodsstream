/*
    libodsstream is a library to read and write ODS documents as streams
    Copyright (C) 2013  Olivier Langella <Olivier.Langella@moulon.inra.fr>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

#include <QDebug>
#include "odscell.h"
#include "../odsexception.h"

OdsCell::OdsCell()
{
}

OdsCell::~OdsCell()
{
}

void
OdsCell::setOfficeValueType(const QString &type)
{
  // float
  // percentage
  // currency
  // date
  // time
  // boolean
  // string
  if(type.isEmpty())
    {
      _is_empty = true;
      qDebug() << "empty cell";
      _office_value_type = "";
    }
  else
    {
      _is_empty          = false;
      _office_value_type = type;
    }
  _string_value = "";
};
void
OdsCell::setDateValue(const QDateTime &date)
{
  //   qDebug() << "setDateValue date.fromString " <<
  //   date.toString(Qt::ISODate);
  _date_value   = date;
  _string_value = date.toString();
};

void
OdsCell::setValueString(const QString &value)
{
  _string_value = value;
};
void
OdsCell::setValueDouble(double value_num)
{
  _double_value = value_num;
  _string_value.setNum(value_num);
};
void
OdsCell::setValueBoolean(bool value_bool)
{
  _bool_value = value_bool;
  _string_value.setNum(value_bool);
};

const QString &
OdsCell::toString() const
{
  // if (isDate()) return QString (_date_value.toString());
  // if (isDouble()) return QString(""+_double_value);
  return _string_value;
};

const QString &
OdsCell::getOfficeValueType() const
{
  return _office_value_type;
};

const QDateTime &
OdsCell::getDateTimeValue() const
{
  if(isDate())
    {
      return _date_value;
    }
  else
    {
      throw OdsException(
        QObject::tr("this cell is not a date :\n").append(toString()));
    }
};
const QString &
OdsCell::getStringValue() const
{
  if(isString())
    {
      return _string_value;
    }
  else
    {
      throw OdsException(
        QObject::tr("this cell is not a string :\n").append(toString()));
    }
};
double
OdsCell::getDoubleValue() const
{
  if(isDouble())
    {
      return _double_value;
    }
  else
    {
      throw OdsException(
        QObject::tr("this cell is not a double :\n").append(toString()));
    }
};
bool
OdsCell::getBooleanValue() const
{
  if(isBoolean())
    {
      return _bool_value;
    }
  else
    {
      throw OdsException(
        QObject::tr("this cell is not a boolean :\n").append(toString()));
    }
};
bool
OdsCell::isEmpty() const
{
  return _is_empty;
};
bool
OdsCell::isDate() const
{
  if(_office_value_type == "date")
    return true;
  return false;
};
bool
OdsCell::isDouble() const
{
  if(_office_value_type == "float")
    return true;
  return false;
};
bool
OdsCell::isString() const
{
  if(isDate())
    return false;
  if(isDouble())
    return false;
  if(isBoolean())
    return false;
  if(isEmpty())
    return false;
  return true;
};
bool
OdsCell::isBoolean() const
{
  if(_office_value_type == "boolean")
    return true;
  return false;
};


void
OdsCell::setValueOfUndefinedType(const QString &value)
{
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " "
           << value;
  _office_value_type = "string";
  _string_value      = value;

  // float
  // percentage
  // currency
  // date
  // time
  // boolean
  // string

  bool ok;
  _double_value = _string_value.toDouble(&ok);
  if(ok)
    {
      _office_value_type = "float";
      return;
    }
  else
    {
      // test for french decimal separator
      _double_value = QString(_string_value).replace(",", ".").toDouble(&ok);
      if(ok)
        {
          _office_value_type = "float";
          _string_value      = QString(_string_value).replace(",", ".");
          return;
        }
    }
  if((_string_value == "T") || (_string_value == "TRUE") ||
     (_string_value == "VRAI"))
    {
      _bool_value        = true;
      _office_value_type = "boolean";
      return;
    }
  if((_string_value == "F") || (_string_value == "FALSE") ||
     (_string_value == "FAUX"))
    {
      _bool_value        = false;
      _office_value_type = "boolean";
      return;
    }
  QDateTime date = QDateTime::fromString(_string_value, "yyyy-MM-ddThh:mm:ss");
  if(date.isValid())
    {
      _date_value        = date;
      _office_value_type = "date";
      return;
    }
  date = QDateTime::fromString(_string_value, "yyyy-MM-dd");
  if(date.isValid())
    {
      _date_value        = date;
      _office_value_type = "date";
      return;
    }
  if(!_string_value.isEmpty())
    {
      _is_empty = false;
    }
};
