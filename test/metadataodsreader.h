/**
 * \file /input/mcqr/metadataodsreader.h
 * \date 14/09/2023
 * \author Olivier Langella
 * \brief read metadata ods file
 */

/*******************************************************************************
 * Copyright (c) 2023 Olivier Langella
 *<Olivier.Langella@universite-paris-saclay.fr>.
 *McqrMetadata
 * This file is part of i2MassChroQ.
 *
 *     i2MassChroQ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     i2MassChroQ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with i2MassChroQ.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once


#include <odsstream/odsdocreader.h>
#include <odsstream/odsexception.h>
#include <QStringList>


/**
 * @todo write docs
 */
class MetadataOdsReader : public OdsDocHandlerInterface
{
  public:
  /**
   * Default constructor
   */
  MetadataOdsReader();

  /**
   * Destructor
   */
  ~MetadataOdsReader();


  /**
   * callback that indicates the begining of a data sheet. Override it in
   * order to retrieve information about the current data sheet.
   *
   */
  virtual void startSheet(const QString &sheet_name) override;

  /**
   * callback that indicates the end of the current data sheet. Override it if
   * needed
   */
  virtual void endSheet() override;

  /**
   * callback that indicates a new line start. Override it if needed.
   */

  virtual void startLine() override;

  /**
   * callback that indicates a line ending. Override it if needed.
   */

  virtual void endLine() override;

  /**
   * callback that report the content of the current cell in a dedicated Cell
   * object. Override it if you need to retrieve cell content.
   */
  virtual void setCell(const OdsCell &) override;

  /**
   * callback that report the end of the ODS document. Override it if you need
   * to know that reading is finished.
   */
  virtual void endDocument() override;


  private:
  QStringList m_headers;
  bool m_isContent = false;
  std::size_t m_column;
};
