/**
 * \file src/ziptsvoutputstream.h
 * \date 27/6/2022
 * \author Olivier Langella
 * \brief write TSV files in a zip archive
 */

/*******************************************************************************
 * Copyright (c) 2022 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of libodsstream.
 *
 *     libodsstream is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     libodsstream is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with libodsstream.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


#pragma once

#include "tsvdirectorywriter.h"
#include <QFile>
#include <quazipfile.h>
/**
 * @todo write docs
 */
class ZipTsvOutputStream : public TsvDirectoryWriter
{
  public:
  /**
   * Default constructor
   */
  ZipTsvOutputStream(const QString &zipfilename);


  /**
   * Destructor
   */
  virtual ~ZipTsvOutputStream();

  virtual void writeSheet(const QString &sheetName) override;
  
  
  void close() override;


  private:
  void closeCurrentZipSheet();


  private:
  QFile *mpa_fileZipArchive    = nullptr;
  QuaZip *mpa_quaZip           = nullptr;
  QuaZipFile *mpa_outFileInZip = nullptr;
};
