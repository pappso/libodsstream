message("\n${BoldRed}WIN10-MINGW64 environment${ColourReset}\n")
message("Please run the configuration like this:")
message("cmake -G \"Unix Makefiles\" -DCMAKE_BUILD_TYPE=Release ../../development")

set(MAKE_TEST 0)

if(WIN32 OR _WIN32)
	message(STATUS "Building with WIN32 defined.")
endif()

set(CMAKE_NO_SYSTEM_FROM_IMPORTED = 1)

# Fix for the horrible bug of CMake that does not properly source the contents
# of the CMakeFiles/<lib target>.dir/includes_CXX.rsp response file. This setting makes
# cmake not rely on that file but put all includes directly in the command line
# instead of referring to that response file.
set(CMAKE_CXX_USE_RESPONSE_FILE_FOR_INCLUDES 0)
# Exactly the same for libraries.
set(CMAKE_CXX_USE_RESPONSE_FILE_FOR_LIBRARIES 0)

find_package(Quazip-Qt6 REQUIRED)

#set(QUAZIP_FOUND 1)
#set(QUAZIP_INCLUDE_DIR C:/msys64/mingw64/include/QuaZip-Qt5-1.3)
#set(QUAZIP_LIBRARIES C:/msys64/mingw64/bin/libquazip1-qt5.dll) 
#if(NOT TARGET QUAZIP::QUAZIP)
	#add_library(QUAZIP::QUAZIP UNKNOWN IMPORTED)
	#set_target_properties(QUAZIP::QUAZIP PROPERTIES
		#IMPORTED_LOCATION             ${QUAZIP_LIBRARIES}
		#INTERFACE_INCLUDE_DIRECTORIES ${QUAZIP_INCLUDE_DIR})
#endif()
#message(STATUS Found QUAZIP: ${QUAZIP_LIBRARIES})


find_package(ZLIB REQUIRED)


# On Win10 all the code is relocatable.
remove_definitions(-fPIC)
